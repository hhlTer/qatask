package tests.prestshop_test;

import org.junit.*;
import org.junit.runners.MethodSorters;
import prestashop_automation.model.*;
import prestashop_automation.pages.HomePage;
import prestashop_automation.pages.SearchPage;
import prestashop_automation.utils.TestService;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import java.util.stream.Collectors;
import org.apache.log4j.Logger;


@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class PagesTest extends BaseTest{

    private static HomePage homePage;
    private static SearchPage searchPage;

    private static Logger log = Logger.getLogger(PagesTest.class.getName());

    @BeforeClass
    public static void init(){
        homePage = new HomePage(driver);
    }

    //1.      Открыть главную страницу сайта
    @Test//(testName = "OPEN HOME PAGE")
    public void stage1_homePageTest(){
        log.info("'OPEN HOME PAGE' test started");
        goToHomePage();
        Assert.assertTrue(homePage.isHomePage());
        log.info("'OPEN HOME PAGE' done");
    }

    //2.      Выполнить проверку, что цена продуктов в секции "Популярные товары" указана в соответствии с установленной валютой в шапке сайта (USD, EUR, UAH).
    @Test//(testName = "Checking currency compliance") //TODO: change BL: send the currency for equals
    public void stage2_uahCurrencyEqualsTest(){
        Currency expectedCurrency = Currency.getInstance(CurrencyEnums.UAH);
        currencyEqualsTest(expectedCurrency);
    }

    //3.      Установить показ цены в USD используя выпадающий список в шапке сайта.
    @Test
    public void stage3_changeBaseCurrencyToUSD(){
        log.info("Changing currency to USD' test started");
        homePage.changeCurrency(CurrencyEnums.USD); //logged
        final Currency EXPECTED_CURRENCY = Currency.getInstance(CurrencyEnums.USD);
        final Currency CURRENCY_IN_PAGE = homePage.getMainCurrency(); //logged
        Assert.assertEquals(EXPECTED_CURRENCY, CURRENCY_IN_PAGE);
        log.info("Changing currency to USD' test stopped");

    }

    //4.      Выполнить поиск в каталоге по слову “dress.”
    @Test
    public void stage4_searchInCatalogTest(){
        log.info("'Search in catalog' test started");
        searchPage = homePage.searchInCatalog("dress");
        final String CURRENT_URL = homePage.getCurrentUrl();
        Assert.assertTrue(CURRENT_URL.contains("search"));
        Assert.assertTrue(CURRENT_URL.contains("dress"));
        log.info("'Search in catalog' test stopped");
    }

    //5.      Выполнить проверку, что страница "Результаты поиска" содержит надпись "Товаров: x", где x - количество действительно найденных товаров.
    @Test
    public void stage5_testCountOfProduct(){
        log.info("'Coincidence of amount products' test started");
        Integer countOfProduct = searchPage.getCountOfProducts();
        String countOfProductText = searchPage.getCountOfProductsText();

        Integer countFromText = TestService.catIntegerFromString(countOfProductText);
        Assert.assertEquals(countFromText, countOfProduct);
        log.info("'Coincidence of amount products' test stopped");
    }

    //6.      Проверить, что цена всех показанных результатов отображается в долларах.
    @Test
    public void stage6_usdCurrencyEqualsTest(){
        Currency expectedCurrency = Currency.getInstance(CurrencyEnums.USD);
        currencyEqualsTest(expectedCurrency);
    }

    //7.      Установить сортировку "от высокой к низкой."
    //8.      Проверить, что товары отсортированы по цене, при этом некоторые товары могут быть со скидкой, и при сортировке используется цена без скидки.
    @Test
    public void stage7_highToLowestOrderTest(){
        log.info("'High to lower order' test started");
        searchPage.sort(SortingType.HIGH_TO_LOW);
        List<Product> products = searchPage.getProductsFromPage();

        List<BigDecimal> pricesToComparing = products.stream()
                .map(p -> p.isDiscount() ? p.getRegularPrice() : p.getPrice())
                .map(p -> p.getValue())
                .collect(Collectors.toList());

        log.info("Prices in order: ");
        log.info(pricesToComparing.stream()
                .map(p -> p.toString())
                .collect(Collectors.joining(" -> "))

        );

        for (int i = 1; i < pricesToComparing.size(); i++) {
            Assert.assertTrue(pricesToComparing.get(i-1).compareTo(pricesToComparing.get(i)) >= 0);
        }
        log.info("'High to lower order' test stopped");
    }

    //9.   Для товаров со скидкой указана скидка в процентах вместе с ценой до и после скидки.
    //10.  Необходимо проверить, что цена до и после скидки совпадает с указанным размером скидки
    @Test
    public void stage8_discountTest(){
        log.info("'Discounts appropriate' test started");
        List<Product> products = searchPage.getProductsFromPage();
        for (Product p:
             products) {
            if (!p.isDiscount()){
                log.info("Product " + p.getName() + " has no discount");
                continue;
            }

            log.info("Product " + p.getName() + " has a discount");
            BigDecimal price = p.getPrice().getValue();
            BigDecimal regularPrice = p.getRegularPrice().getValue();

            log.info("Price = " + price);
            log.info("Regular price = " + regularPrice);

            int percentDiscount = p.getDiscountPercent();
            log.info("Current discount = " + percentDiscount);

            int expectedPercentDiscount = calculateDiscountByPercent(price, regularPrice);

            log.info("Manually counting of discount = " + expectedPercentDiscount);
            log.info(percentDiscount + " = " + expectedPercentDiscount + " is " + (percentDiscount == expectedPercentDiscount));
            Assert.assertEquals(expectedPercentDiscount, percentDiscount);
        }
        log.info("'Discounts appropriate' test stopped");
    }

    private int calculateDiscountByPercent(BigDecimal price, BigDecimal regularPrice) {
        return price
        .multiply(new BigDecimal(100))
        .divide(regularPrice, 2, RoundingMode.DOWN)
        .subtract(new BigDecimal(100))
        .abs()
        .setScale(0, RoundingMode.HALF_DOWN)
        .intValue();
    }

    private void goToHomePage(){
        if (!homePage.isHomePage()) {
            homePage.goToHomePage();
        }
    }

    private void currencyEqualsTest(Currency expectedCurrency) {
        log.info("'Checking currency compliance' test started");
        Currency baseCurrency = homePage.getMainCurrency();

        Assert.assertSame(expectedCurrency, baseCurrency);
        log.info("Main currency is equals to " + baseCurrency.getCurrencyName());

        List<Currency> currencyList = homePage.getProductCurencies();

        for (Currency c:
                currencyList) {
            Assert.assertSame(c, baseCurrency);
        }
        log.info("Checking currency compliance' test stopped");
    }

}
