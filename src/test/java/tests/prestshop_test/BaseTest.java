package tests.prestshop_test;

import org.apache.log4j.Logger;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import tests.driver.Driver;
import tests.driver.DriversEnum;

public class BaseTest {

    static WebDriver driver;

    private static Logger log = Logger.getLogger(BaseTest.class.getName());

    @BeforeClass
    public static void setup(){
//        driver = new ChromeDriver();
        driver = Driver.getInstance(DriversEnum.CHROME);
        driver.manage().window().maximize();
        Capabilities cap = ((RemoteWebDriver) driver).getCapabilities();
        String browserName = cap.getBrowserName().toLowerCase();
        String os = cap.getPlatform().toString();
        log.info("Init driver");
        log.info("Browser: " + browserName);
        log.info("System: " + os);
    }

    @AfterClass
    public static void quit(){
        driver.quit();
        log.info("Quit");
    }

}
