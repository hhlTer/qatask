package prestashop_automation.model;

import java.math.BigDecimal;

public class Price {
    private BigDecimal price;
    private Currency currency;

    public BigDecimal getValue() {
        return price;
    }

    public Price setPrice(BigDecimal price) {
        this.price = price;
        return this;
    }

    public Currency getCurrency() {
        return currency;
    }

    public Price setCurrency(Currency currency) {
        this.currency = currency;
        return this;
    }
}