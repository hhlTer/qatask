package prestashop_automation.model;

public enum CurrencyEnums {
    UAH("₴"), EUR("€"), USD("$");

    private String sign;
    private String ruDescription;

    CurrencyEnums(String sign){
        this.sign = sign;
        ruDescription = sign.equals("$") ? "Доллар США" :
                        sign.equals("₴") ? "Украинская гривна" :
                        sign.equals("€") ? "Евро" : null;
    }

    public String getSign() {
        return sign;
    }
    public String getRuDescription() { return ruDescription;}

    public static CurrencyEnums getInstance(String sign){
        return sign.equals("$") ? USD :
               sign.equals("€") ? EUR :
               sign.equals("₴") ? UAH : null;
    }

}