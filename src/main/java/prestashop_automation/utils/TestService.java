package prestashop_automation.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TestService {

    private static final Pattern NUMBERS_PATTERN = Pattern.compile("[0-9]+");

    public static Integer catIntegerFromString(String text){ //TODO: test

        Integer result = null;
        Matcher matcher = NUMBERS_PATTERN.matcher(text);
        while (matcher.find()){
            result = Integer.valueOf(matcher.group());
            break;
        }

        return result;
    }
}
