package prestashop_automation.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;

import prestashop_automation.model.SortingType;
public class SearchPage extends BasePage {

    private static final By ARTICLE_FOR_EACH_PRODUCT = By.cssSelector("article[class='product-miniature js-product-miniature']");
    private static final By COUNT_OF_PRODUCTS = By.cssSelector("div[class='col-md-6 hidden-sm-down total-products']");
    private static final By SORT_DROPDOWN = By.cssSelector("div[class='col-sm-12 col-xs-12 col-md-9 products-sort-order dropdown']");

    private static By chooseSortingType;

    SearchPage(WebDriver driver) {
        super(driver);
    }

    public Integer getCountOfProducts() {
        System.out.println();
        List<WebElement> elements = getElements(ARTICLE_FOR_EACH_PRODUCT);
        return elements.size();
    }

    public void sort(SortingType sortingType){
        click(SORT_DROPDOWN);
        chooseSortingType = By.xpath("//*[contains(@href, '" + sortingType.getDescription() + "')]"); //TODO che
        click(chooseSortingType);
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public String getCountOfProductsText() {
        return getText(COUNT_OF_PRODUCTS);
    }

}
