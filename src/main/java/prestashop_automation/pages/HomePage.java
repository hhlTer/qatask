package prestashop_automation.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import prestashop_automation.model.Currency;
import prestashop_automation.model.CurrencyEnums;
import prestashop_automation.model.Product;

import java.util.List;
import java.util.stream.Collectors;

public class HomePage extends BasePage{

    private static Logger log = Logger.getLogger(HomePage.class.getName());

    private static final String BASE_URL = "http://prestashop-automation.qatestlab.com.ua/ru/";
    private static By CURRENCY_CHOOSE_IN_MENU = null;

    public HomePage(WebDriver driver) {
        super(driver);
    }

    public void goToHomePage(){
        log.info("Open page " + BASE_URL);
        driver.get(BASE_URL);
        log.info(driver.getCurrentUrl() + " opened successfully");
    }

    public boolean isHomePage(){
        return driver.getCurrentUrl().equals(BASE_URL);
    } //TODO: log

    public Currency getMainCurrency(){
        log.info("Search base currency sign on page by selector: + '" + CURRENCY_SELECTOR + "'");
        String currencySign = getText(CURRENCY_SELECTOR);
        log.info("Base currency with text " + currencySign + " successfully founded");
        return Currency.getInstanceBySign(currencySign);
    }

    /**
     * find all product proposals on page and
     * @return all currencies from prices (using currency sign in text)
     */
    public List<Currency> getProductCurencies() {
        log.info("Search all currencies in product list");

        List<Product> products = getProductsFromPage();

        List<Currency> currencies = products.stream()
                .map(p -> p.getPrice().getCurrency())
                .collect(Collectors.toList());
        log.info(currencies.size() + " currency(ies) successfully founded");
        return currencies;
    }

    public void changeCurrency(CurrencyEnums currency) {
        //call dropdown menu
        log.info("Click on currency dropdown menu calling");
        click(CURRENCY_DROPDOWN);
        log.info("Click on currency dropdown menu done");

        //choose currency
        log.info("Click on currency in dropdown menu calling");
        CURRENCY_CHOOSE_IN_MENU = By.xpath(".//a[@title='" + currency.getRuDescription() + "']");
        click(CURRENCY_CHOOSE_IN_MENU);
        log.info("Click on currency in dropdown menu done");
    }

    public SearchPage searchInCatalog(String searchText) {
        log.info("Search products in catalog '" + searchText + "'");
        setText(SEARCH_TEXTFIELD, searchText);
        submit(SEARCH_TEXTFIELD);
        log.info("Searching successfully completed");
        return new SearchPage(driver);
    }
}
