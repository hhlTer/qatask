package prestashop_automation.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import prestashop_automation.model.Product;
import prestashop_automation.service.ProductService;

import java.util.ArrayList;
import java.util.List;

public class BasePage {

    private Logger log = Logger.getLogger(BasePage.class.getName());

    private static final int TIMEOUT_IN_SEC = 14;
    private static final By PRODUCTS = By.className("product-description");
    static final By CURRENCY_SELECTOR = By.id("_desktop_currency_selector");
    static final By CURRENCY_DROPDOWN = By.xpath(".//div[@class='currency-selector dropdown js-dropdown']/a[@data-toggle='dropdown']");
    static final By SEARCH_TEXTFIELD = By.name("s");


    WebDriver driver;
    private WebDriverWait wait;
    private ProductService productService = ProductService.getInstance();

    BasePage(WebDriver driver){
        log.info("init driver " + driver.toString());
        this.driver = driver;
        this.wait = new WebDriverWait(driver, TIMEOUT_IN_SEC);
    }

    private void waitVisibility(By by){
        wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(by));
    }
    private void waitVisibility(WebElement element){
        wait.until(ExpectedConditions.visibilityOf(element));
    }

    void click(By element){
        waitVisibility(element);
        log.info("Click on element: '" + element + "'");
        try {
            getElement(element).click();
        } catch (NoSuchElementException e){
            log.error("Element " + element.toString() + " no found", e);
        }
        log.info("'" + element + "' clicked successfully!");
    }

    private WebElement getElement(By by) throws NoSuchElementException {
        log.info("Search element: '" + by.toString() + "'");
        waitVisibility(by);
        try {
            return driver.findElement(by);
        } catch (NoSuchElementException e){
            log.warn("Element '" + by.toString() + "' not found: " + e.getLocalizedMessage());
            throw new NoSuchElementException(e.getMessage());
        }
    }

    List<WebElement> getElements(By by){
        log.info("Search element: '" + by.toString() + "'");
        waitVisibility(by);
        try {
            return driver.findElements(by);
        } catch (NoSuchElementException e){
            log.warn("Element '" + by.toString() + "' not found: " + e.getLocalizedMessage());
            throw new NoSuchElementException(e.getMessage());
        }
    }

    String getText(By by) throws NoSuchElementException {
        log.info("Get text from element by " + by.toString());
        waitVisibility(by);
        String result;
        try {
            result = getElement(by).getText();
        } catch (NoSuchElementException e){
            log.warn("Search element " + by + " manually detected error");
            result = "";
        }
        log.info("text() = \"" + result + "\"");
        return result;
    }

    private String getText(WebElement element, By by){
        waitVisibility(element);
        String result;
        log.info("Get text from element by " + by.toString());
        try {
            result = element.findElement(by).getText();
        } catch (org.openqa.selenium.NoSuchElementException e){
            log.warn("Search element " + by + " manually detected error");
            result = "";
        }
        log.info("text() = \"" + result + "\"");
        return result;
    }

    void setText(By by, String text){
        log.info("Set text " + text + " to element " + by.getClass().getName());
        waitVisibility(by);
        getElement(by).sendKeys(text);
        log.info("Text successfully assigned");
    }

    void submit(By by){
        log.info("Submit element '" + by.toString() + "'");
        waitVisibility(by);
        getElement(by).submit();
        log.info("Element " + by.toString() + " successfully submitted");
    }

    public String getCurrentUrl() {
        return driver.getCurrentUrl();
    }

    public List<Product> getProductsFromPage() {

        log.info("Finding products ");
        List<WebElement> webElements = getElements(PRODUCTS);
        List<Product> products = new ArrayList<>();

        for (WebElement e:
                webElements) {
            String price = getText(e, By.className("price"));
            String regularPrice = getText(e, By.className("regular-price"));
            String name = getText(e, By.tagName("h1"));
            String percentage = getText(e, By.className("discount-percentage"));

            Product p = productService.fillAndGetProduct(name, price, regularPrice, percentage);

            products.add(p);
        }

        return products;
    }

}
