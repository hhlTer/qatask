package prestashop_automation.service;

import prestashop_automation.model.Price;
import prestashop_automation.model.Product;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ProductService {

    private PriceService priceService = PriceService.getInstance();

    private ProductService(){

    }

    private static ProductService instance;

    public static ProductService getInstance(){
        if (instance == null){
            instance = new ProductService();
        }

        return instance;
    }

    /**
     * @param name - name of product
     * @param initPrice native price string from 'http://prestashop-automation.qatestlab.com.ua/ru/'
     * @param initRegularPrice - native regular price string from 'http://prestashop-automation.qatestlab.com.ua/ru/'
     *                         initRegularPrice is empty if product no have the discount
     * @return Product instance.
     */
    public Product fillAndGetProduct(String name, String initPrice, String initRegularPrice, String discountPercent){
        Product result = new Product();
        Price price;
        Price regularPrice;

        price = priceService.normalizeStringAndGetPrise(initPrice);
        regularPrice = priceService.normalizeStringAndGetPrise(initRegularPrice);

        result.setRegularPrice(regularPrice);
        result.setName(name);
        result.setPrice(price);
        result.setDiscountPercent(normalizeStringToInt(discountPercent));

        return result;
    }

    private int normalizeStringToInt(String stringWithIntValue){
        if (stringWithIntValue == null || stringWithIntValue.length() < 1){
            return 0;
        }
        Pattern numbers = Pattern.compile("[0-9]+");
        Matcher matcher = numbers.matcher(stringWithIntValue);
        int result = 0;

        while (matcher.find()){
            result = Integer.valueOf(matcher.group());
        }

        return result;
    }
}
